const exceptionKeys = require('./filter-key-words.const').exceptionKeys;
const fieldsCollection = require('./filter-key-words.const').fieldsCollection;

module.exports = (requestParams) => {
    const params = {};

    for (let [key, value] of requestParams) {
        if (!key.includes(exceptionKeys)) {
            const foundField = fieldsCollection.find((field) => field.name === key);

            params[key] = `{ ${foundField.mongoose_syntax}: ${value} }`;
        }
    }

    return params;
};
