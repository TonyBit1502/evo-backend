module.exports.exceptionKeys = ['page', 'per_page'];

module.exports.fieldsCollection = [
    { name: 'start_with', mongoose_syntax: '$regex' },
    { name: 'name', mongoose_syntax: '$search' },
    { name: 'great_than', mongoose_syntax: '$gt' },
    { name: 'great_than_or_equal', mongoose_syntax: '$gte' },
    { name: 'less_than', mongoose_syntax: '$lt' },
    { name: 'less_than_or_equal', mongoose_syntax: '$lte' },
    { name: 'from_date', mongoose_syntax: '$gt' },
    { name: 'to_date', mongoose_syntax: '$lt' },
    { name: 'from_date_with', mongoose_syntax: '$gte' },
    { name: 'to_date_with', mongoose_syntax: '$lte' },
];
