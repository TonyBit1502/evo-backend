const { Schema, model } = require('mongoose');

const schema = new Schema({
    login: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: new Date
    },
    updated_at: {
        type: Date,
        default: null
    },
    deleted_at: {
        type: Date,
        default: null
    },
    first_name: {
        type: String,
        default: null
    },
    last_name: {
        type: String,
        default: null
    },
    middle_name: {
        type: String,
        default: null
    },
    roles: {
        type: [String],
        default: ['visitor']
    }
});

module.exports.User = model('users', schema);
