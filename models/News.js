const { Schema, model } = require('mongoose');
const schema = new Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    author: {
        type: String,
        required: true
    },
    is_published: {
        type: Boolean,
        default: false
    },
    image_urls: {
        type: [String],
        default: []
    },
    updated_at: {
        type: Date,
        default: null
    },
    published_at: {
        type: Date,
        default: null
    },
    deleted_at: {
        type: Date,
        default: null
    }
});

module.exports = model('news', schema);
