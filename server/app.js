require('dotenv/config');
const bodyParser = require('body-parser');
const connectRedis = require('connect-redis');
const express = require('express');
const session = require('express-session');
const Redis = require('ioredis');

const {
    NODE_ENV = 'development',
    REDIS_DB_NAME = 'redis_database',
    REDIS_HOST = 'localhost',
    REDIS_PORT = 6379,
    REDIS_PASSWORD = 'redis_secret',
    SESS_LIFETIME = 60000,
    SESS_NAME = 'sid',
    SESS_SECRET = 'session_secret',
} = process.env;

/**
 * Для использования Redis нужно установить redis-server и запустить его
 * Инструкция для установки сервера:
 * https://redis.io/topics/quickstart
 *
 * https://github.com/tj/connect-redis
 * https://github.com/alex996/presentations/blob/master/express-session.md
 */
const RedisStore = connectRedis(session);

const client = new Redis({
    host: REDIS_HOST,
    port: Number(REDIS_PORT),
    password: REDIS_PASSWORD
});

const store = new RedisStore({ client });

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(
    session({
        cookie: {
            maxAge: Number(SESS_LIFETIME),
            sameSite: true,
            secure: NODE_ENV === 'production'
        },
        name: SESS_NAME,
        resave: false,
        rolling: true,
        saveUninitialized: false,
        secret: SESS_SECRET,
        store
    })
);

module.exports = app;
