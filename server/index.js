require('dotenv/config');
const mongoose = require('mongoose');
const { PORT = 4000, DB_CONNECT } = process.env;

const app = require('./app');
const appRoutes = require('../routes/index');
const authHandler = require('../middlewares/auth-handler');
const bodyHandler = require('../middlewares/request-body-handler');

start();

async function start() {
    try {
        await mongoose.connect(DB_CONNECT, {
            useNewUrlParser: true,
            useFindAndModify: false,
            useUnifiedTopology: true
        });

        for (let appRoutesItem of appRoutes) {
            if (appRoutesItem.isJSL) {
                app.use(`${appRoutesItem.endpoint}/api`, bodyHandler, appRoutesItem.routes);
            } else {
                app.use(`${appRoutesItem.endpoint}/api`, authHandler, bodyHandler, appRoutesItem.routes);
            }
        }

        app.listen(PORT, () => {
            console.log('Server has been started....');
        });
    } catch(err) {
        console.error(err);
    }
}
