module.exports = (req, res, next) => {
    if (!req.body) {
        res.status(400).json({
            error: 'Bad request. Request body not found',
            status: 400
        });
    } else {
        next();
    }
};
