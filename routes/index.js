const authRoutes = require('./session.routes');
const newsRoutes = require('./news.routes');
const userRoutes = require('./user.routes');

module.exports = [
    { endpoint: '/jsl', routes: authRoutes, isJSL: true },
    { endpoint: '/core', routes: newsRoutes, isJSL: false },
    { endpoint: '/core', routes: userRoutes, isJSL: false }
];
