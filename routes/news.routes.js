const { Router } = require('express');
const News = require('../models/News');
const router = Router();

router.get('/news', async (req, res) => {
    try {
        const news = await News.find();
        res.json(news);
    } catch (err) {
        res.status(500).json({
            message: err.message,
            status: 500
        })
    }
});

router.get('/news/:id', async (req, res) => {

});

router.post('/news/create', async (req, res) => {
    try {
        const news = new News({
            title: req.body.title,
            description: req.body.description,
            author: req.body.author,
        });

        await news.save();

        res.status(201).json(news);
    } catch(err) {
        res.status(500).json({
            message: err.message,
            status: 500
        })
    }
});

router.put('/news/:id', async (req, res) => {

});

router.delete('/news/:id', async (req, res) => {

});

module.exports = router;
