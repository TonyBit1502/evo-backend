const { Router } = require('express');
const bcrypt = require('bcrypt-nodejs');
const User = require('../models/User').User;
const router = Router();

router.get('/users', async(req, res) => {
    const page = parseInt(req.body.page) || 1;
    const per_page = parseInt(req.body.per_page) || 50;

    if (page <= 0) {
        res.status(400).json({
            message: 'Error! Invalid page number',
            status: 400
        });
    }

    try {
        const params = {};
        const options = {};

        /**
         * TODO доработать перечитывание параметров запроса.
         * Предлагаемый вариант - реализация отдельного метода-helper'а
         * В нем будет итерироваться массив массивов, передаваемый как входной параметр (Object.entries(req.body): ([[key, value], ...]) )
         * Для того, чтобы mongoose понял, как именно следует производить выборку, необходимо составить коллекцию ключевых слов, по которым будет проверяться значение key на каждой итерации цикла.
         * В зависимости от этого будет мутироваться значение value, что позволит корректно сгенерировать запрос к базе и получить желаемый результат.
         * Также это приведет фильтры на бэкенде к единообразию.
         * За основу можно взять перебор массива представленный ниже.
         */
        for (let [key, value] of Object.entries(req.body)) {
            if (key !== 'page' && key !== 'per_page') {
                params[key] = value;
            }
        }

        options.skip = per_page * (page - 1);
        options.limit = per_page;

        const totalCount = await User.countDocuments(params);
        const result = await User.find(params, '-password', options);
        const totalPages = Math.ceil(totalCount / per_page);

        res.setHeader('Page', page);
        res.setHeader('Total-Pages', totalPages);
        res.json(result);
    } catch(err) {
        res.status(500).json({
           message: err.message,
           status: 500
        });
    }
});

router.get('/users/:id', async(req, res) => {
    try {
        const user = await User.findById(req.params.id, '-password');

        res.json(user);
    } catch(err) {
        res.status(500).json({
            message: err.message,
            status: 500
        });
    }
});

router.post('/users', async(req, res) => {
    try {
        const {login, password} = req.body;
        const candidate = await User.findOne({login});

        if (candidate) {
            res.status(409).json({
                message: 'Login already exists',
                status: 409
            });
        }

        const user = new User({
            login,
            password: bcrypt.hashSync(password, bcrypt.genSaltSync(10))
        });

        await user.save();

        res.status(201).json(user);
    } catch(err) {
        res.status(500).json({
            message: err,
            status: 500
        })
    }
});

router.put('/users/:id', async(req, res) => {
    try {

    } catch(err) {

    }
});

router.delete('/users/:id', async(req, res) => {
    try {

    } catch(err) {

    }
});

module.exports = router;
