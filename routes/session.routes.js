const { Router } = require('express');
const bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');
const User = require('../models/User').User;
const { SESS_LIFETIME = 60000 } = process.env;

const router = Router();

/**
 * Для улучшения читаемости кода необходимо вынести работу с сессиями (get, set, all, destroy)
 * в отдельный файл и импортировать его.
 */
router.post('/session/login', (req, res) => {
   const {login, password, auth_token} = req.body;

   // req.sessionStore.clear((error) => console.log(error));

   req.sessionStore.all(async (err, sessions) => {
      if (err) {
         console.log('GET_SESSION_ERROR: ', err);
         return;
      }

      if (auth_token) {
         const foundSession = sessions.find((session) => {
            return session.token === auth_token;
         });

         if (foundSession) {
            res.json(foundSession);

            return;
         }
      }

      try {
         const candidate = await User.findOne({login});

         if (candidate) {
            const matchPassword = bcrypt.compareSync(password, candidate.password);

            if (matchPassword) {
               const token = jwt.sign({
                  login: candidate.login,
                  userId: candidate._id
               }, process.env.JWT_SECRET, { expiresIn: Number(SESS_LIFETIME) });

               const session = {
                  id: req.session.id,
                  roles: candidate.roles,
                  token,
                  user_id: candidate._id
               };

               req.sessionStore.set(req.session.id, session, (err) => {
                  console.log('SET_SESSION_ERROR: ', err);
               });

               res.json(session);
            }
         } else {
            res.status(404).json({
               message: 'User not found',
               status: 404
            })
         }
      } catch (err) {
         res.status(500).json({
            message: "Error! " + err,
            status: 500
         });
      }
   });
});

router.post('/session/logout', (req, res) => {
   req.session.destroy((err) => {
      if (err) {
         console.log(err);
      }

      res.send(true);
   });
});

module.exports = router;
